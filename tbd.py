#!/usr/bin/env python3

from daemon import Daemon
from sessions import Sessions
import sys

#import pdb
import socket, threading

from flask import Flask, request, Response, jsonify
from gevent.pywsgi import WSGIServer
import json
import requests
import re
import time
import urllib

IP_ADDR_SRV  = 'IPSrever'
#Ports currently supported for Webhooks: 443, 80, 88, 8443
PORT_SRV     = 8443
TOKEN_API    = 'token'
CERT_FILE    ='/etc/letsencrypt/live/botest.top/fullchain.pem'
KEY_FILE     ='/etc/letsencrypt/live/botest.top/privkey.pem'
WEBHOOK_ADDR = 'https://botest.top:' + str(PORT_SRV) + '/'
TEL_API_URL  = 'https://api.telegram.org/bot'+TOKEN_API+'/'

PID_FILE     = '/var/run/daemons/bot-ots-telegram.pid'
API_URL      = 'API'

PROP_LOG    = '/home/cel/tel_bot/proposition.txt'
WIZARD_LOG  = '/home/cel/tel_bot/wizard.txt'
LOG_FILE    = '/home/cel/tel_bot/log.txt'
PATH        = '/home/cel/tel_bot/sessions/'
MY_ID       = 'ID'
GROUP_ID    = ID

SOCKET_ADDR = 'localhost'
SOCKET_PORT = 65534

app = Flask(__name__)
session = Sessions(PATH)

@app.route('/', methods=['GET','POST'])
def incoming():
    if request.method == 'POST':
        response_data = request.get_json()
        write_log(response_data)
        if response_data.get('message'):
            chat_id = response_data['message']['chat']['id']
            user_id = response_data['message']['from']['id']
            name = response_data['message']['from']['first_name'] + ' ' +response_data['message']['from'].get('last_name','')
            message_text = response_data['message'].get('text','')
            callback_query_id = ''
        else:
            chat_id = response_data['callback_query']['message']['chat']['id']
            user_id = response_data['callback_query']['from']['id']
            name = response_data['callback_query']['from']['first_name'] + ' ' +response_data['callback_query']['from'].get('last_name','')
            message_text = response_data['callback_query']['data']
            callback_query_id = response_data['callback_query']['id']
        analyse_message(chat_id, message_text, name, user_id, callback_query_id)
        return Response(status=200)
    else:
        resp = Response('<h1>Вас вітає бот</h1>')
        return resp

def the_part_of_day():
    time_struct = time.localtime(time.time())
    hour = time_struct.tm_hour
    if hour < 4:
        result = 'Доброї ночі!'
    elif hour <=10:
        result = 'Доброго ранку!'
    elif hour <=17:
        result = 'Доброго дня!'
    elif hour <=22:
        result = 'Доброго вечора!'
    else:
        result = 'Доброї ночі!'
    return result

def analyse_message(chat_id, message_text, name='', user_id='', callback_query_id=''):
    if callback_query_id:
        send_answer_callback(callback_query_id)
    if chat_id < 0:
        send_message(GROUP_ID, 'Нажаль, мені заборонено спілкуватись в групах')
        return
    res =session.read_session(chat_id)
    session.write_session(chat_id, 'NONE')
    if res =='SET_PROPOSITION':
        with open(PROP_LOG, 'a') as log:
            log_record = {'text': message_text, 'name':name, 'id': user_id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        txt = 'Пропозиція від ' + name + '\n'+message_text
        send_message(MY_ID, txt)
        key = create_inline_key(yes_no=True)
        send_message(chat_id, 'Дякую! 😊\nЯ передам вашу думку керівництву.\nУ вас ще є запитання?', key)
        return
    if res =='SET_CALL_ORDER':
        with open(WIZARD_LOG, 'a') as log:
            log_record = {'text': message_text, 'name':name, 'id': user_id, 'time':time.asctime()}
            log.write(str(log_record)+',\n')
        txt = 'Виклик від ' + name + '\n'+message_text
        send_message(GROUP_ID, txt)
        key = create_inline_key(yes_no=True)
        send_message(chat_id, "Дякую! 😊\nЯ вже передав все нашим співробітникам. Найближчим часом ми з вами зв'яжимось.\nУ вас ще є запитання?", key)
        return
    message_text = message_text.upper()
    if res =='GET_ORDER_INFO':
        tx = message_text.replace('K', 'К').replace('B', 'В')
        rr = re.search(r'\bКВ-\d{7}', tx)
        if rr:
            query= API_URL+'/'+urllib.parse.quote(rr[0])+'/123'
            resp = requests.get(query)
            if resp.status_code == 200:
                resp_text = resp.json()['status']
            else:
                resp_text = 'Сталася помилка під час обробки запиту. Спробуйте знову.'
            send_message(chat_id, resp_text) 
        else:
            resp_text = "Введене вами значення не є номером квитанції\nФормат номера квитанції 'КВ-0000000'"
            send_message(chat_id, resp_text)
        key = create_inline_key(yes_no=True)
        send_message(chat_id, 'У вас ще є запитання?', key)
        return
    tx = re.search(r'\bПРИВІТ', message_text)
    if tx:
        resp_text = the_part_of_day()
        key = create_inline_key()
        send_message(chat_id,message_text=resp_text+' Чим можу допомогти?', reply_markup=key)
        return
    if message_text=='/START':
        key = create_inline_key()
        send_message(chat_id,message_text="Приємно познайомитись!\nЯ бот компанії 'ОфісТехСервіс'😊", reply_markup=key)
        return
    elif message_text=='GET_CONTACTS':
        send_answer_callback(callback_query_id)
        send_contact(chat_id)
        return
    elif message_text=='GET_WORKS_TIME':
        send_answer_callback(callback_query_id)
        send_work_time(chat_id)
        return
    elif message_text=='GET_MAP':
        send_answer_callback(callback_query_id)
        send_map(chat_id)
        return
    elif message_text=='GET_ORDER_INFO':
        session.write_session(chat_id, 'GET_ORDER_INFO')
        send_message(chat_id,message_text='Введіть номер квитанції')
        return
    elif message_text=='SET_PROPOSITION':
        session.write_session(chat_id, 'SET_PROPOSITION')
        send_message(chat_id,message_text="Напишіть будь ласка свою пропозицію, і ми її обов'язково проаналізуємо. 😊")
        return
    elif message_text=='SET_CALL_ORDER':
        session.write_session(chat_id, 'SET_CALL_ORDER')
        send_message(chat_id,message_text="Опишить будь ласка свою проблему та залиште номер телефона для зв'язку. 😊")
        return
    elif message_text=='SET_YES':
        key = create_inline_key()
        send_message(chat_id,message_text="Ок. Чим можу допомогти?", reply_markup=key)
        return
    elif message_text=='SET_NO':
        send_message(chat_id,message_text='Радий був допомогти. 😊\nЯкщо щось знадобиться - просто привітайтесь зі мною, або посміхніться. 😊')
        return
    elif message_text=='😊':
        key = create_inline_key()
        send_message(chat_id,message_text='Привіт! Я тут.😊\nЩо бажаєте?', reply_markup=key)
    else:
        key = create_inline_key(yes_no=True)
        send_message(chat_id, 'На жаль, я вас не зрозумів.😕\nУ вас ще є запитання?', key)
    return

def send_answer_callback(callback_id):
    url = TEL_API_URL + 'answerCallbackQuery'
    js = {'callback_query_id':callback_id}
    r = requests.post(url, json=js)
    return

def send_map(chat_id):
    url = TEL_API_URL + 'sendLocation'
    loc = {'chat_id':chat_id, 'latitude':48.758630, 'longitude':30.205919}
    r = requests.post(url, json=loc)
    key = create_inline_key(yes_no=True)
    send_message(chat_id, 'У вас ще є запитання?', key)
    return

def send_work_time(chat_id):
    key = create_inline_key(yes_no=True)
    send_message(chat_id, 'Пн-Пт      9:00-17:00\nСб            10:00-14:00\nНеділя     Вихідний\n\nА я відповідаю на питання 24/7! 😊', key)
    return

def send_contact(chat_id):
    key = create_inline_key(yes_no=True)
    send_message(chat_id, '+380474434407\n+380474439874\n+380674736269\nУ вас ще є запитання?', key)
    return

def create_inline_key(yes_no=False):
    if yes_no:
        key = {'inline_keyboard':[[{'text':'Так', 'callback_data':'SET_YES'},
        {'text':'Ні', 'callback_data':'SET_NO'}]]}
    else:
        key = {'inline_keyboard':[[{'text':'Контакти', 'callback_data':'GET_CONTACTS'},
        {'text':'Графік роботи', 'callback_data':'GET_WORKS_TIME'}],
        [{'text':'Перебіг ремонту', 'callback_data':'GET_ORDER_INFO'},
        {'text':'Карта', 'callback_data':'GET_MAP'}],
        [{'text':'Побажання, пропозиції та відгуки', 'callback_data':'SET_PROPOSITION'},
        {'text':'Виклик майстра', 'callback_data':'SET_CALL_ORDER'}
        ]]}
    return key

def write_log(data):
    with open(LOG_FILE, 'a') as log:
        json.dump(data, log, indent = 2, ensure_ascii=False)
    return

def send_message(chat_id, message_text='', reply_markup={}):
    url = TEL_API_URL + 'sendMessage'
    answer = {'chat_id':chat_id, 'text':message_text, 'reply_markup':reply_markup}
    r = requests.post(url, json=answer)
    return r

def run_socketworker(S_ADDR, S_PORT):
    ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss.bind((S_ADDR,S_PORT))
    ss.listen(1)
    while 1:
        conn, addr = ss.accept()
        try:
            data = conn.recv(16384)
            if data!=b'':
                txt = data.decode("utf-8")
                send_message(GROUP_ID,txt)
                with open(WIZARD_LOG, 'a') as log:
                    log_record = {'text': txt, 'name':'viber', 'id': 'viber', 'time':time.asctime()}
                    log.write(str(log_record)+',\n')
                conn.send(b'1')
        finally:
            conn.close()

class MyDaemon(Daemon):
    def run(self):
        #socketworker = threading.Thread(name='socketworker',target=run_socketworker,args=(SOCKET_ADDR, SOCKET_PORT))
        #socketworker.start()
        http_server = WSGIServer((IP_ADDR_SRV, PORT_SRV), app, certfile=CERT_FILE, keyfile=KEY_FILE)
        http_server.serve_forever()

if __name__ == '__main__':
    myDaemon = MyDaemon(PID_FILE)
    if len(sys.argv) ==2:
        if 'start'==sys.argv[1]:
            myDaemon.start()
        elif 'stop'==sys.argv[1]:
            myDaemon.stop()
        elif 'restart'==sys.argv[1]:
            myDaemon.restart()
        else:
            print("Unknown command")
            print("usage: %s start|stop|restart" % sys.argv[0])
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)