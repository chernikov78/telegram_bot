import os

class Sessions:

    def __init__(self, path):
        self.path = path

    def write_session(self, session_id, value):
        with open(self.path + str(session_id) + '.ss', 'w') as log:
            log.write(value)
        return

    def read_session(self, session_id):
        res = ''
        path_session = self.path + str(session_id) + '.ss'
        if os.path.exists(path_session):
            with open(path_session, 'r') as log:
                res = log.read()
        return res